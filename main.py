def input_dishes():
    print('\nДля выходы введите exit\n')
    cook_book = get_cook_book()
    dishes = []

    for i, dish in enumerate(cook_book.keys()):
        print(f'{i + 1}. {dish}')

    while True:
        number = input('\nВведи номер блюда, которое хочешь приготовить: ')

        if number == 'exit':
            break

        number = int(number)
        names_dishes = list(cook_book.keys())

        try:
            dishes.append(names_dishes[number - 1])
            print(names_dishes[number - 1])
        except Exception as e:
            print('Что-то пошло не так')

    return dishes


def get_shop_list_by_dishes(dishes):
    person_count = int(input('\nВведи количество человек: '))
    cook_book = get_cook_book()
    shop_list = dict()

    for dish in dishes:
        ingredients = cook_book[dish]

        for ingredient in ingredients:
            if ingredient["ingredient_name"] in shop_list:
                shop_list[ingredient["ingredient_name"]]["quantity"] += (ingredient["quantity"] * person_count)
            else:
                shop_list[ingredient["ingredient_name"]] = {
                    "measure": ingredient["measure"],
                    "quantity": ingredient["quantity"] * person_count
                }

    print(shop_list)


def get_cook_book():
    with open("recipes.txt") as f:
        cook_book = dict()

        for line in f:
            line = line.strip()
            cook_book[line] = []
            count_ingredient = int(f.readline())
            count = 0

            while count < count_ingredient:
                ingredient = f.readline().strip().split('|')
                cook_book[line].append({
                    "ingredient_name": ingredient[0].strip(),
                    "quantity": int(ingredient[1].strip()),
                    "measure": ingredient[2].strip(),
                })
                count += 1

            f.readline()

        return cook_book


get_shop_list_by_dishes(input_dishes())
